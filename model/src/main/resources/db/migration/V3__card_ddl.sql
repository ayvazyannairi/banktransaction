CREATE TABLE IF NOT EXISTS `card`
(
    `id`         BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `cardType`   VARCHAR(50) ,
    `amount`     DECIMAL,
    `user_id` BIGINT,
    `created_at` DATETIME,
    CONSTRAINT fk_users_cards FOREIGN KEY (user_id)
        REFERENCES users (id)
);
