package com.egs.bank.dto;

/**
 * User data transfer to object.
 * */
public class UserDto {

  private Long id;
  private String uuid;
  private String username;
  private String email;
  private String password;
  private String resetToken;
  private String newPassword;
  private String newPassword1;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getUuid() {
    return uuid;
  }

  public void setUuid(String uuid) {
    this.uuid = uuid;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getResetToken() {
    return resetToken;
  }

  public void setResetToken(String resetToken) {
    this.resetToken = resetToken;
  }

  public String getNewPassword() {
    return newPassword;
  }

  public void setNewPassword(String newPassword) {
    this.newPassword = newPassword;
  }

  public String getNewPassword1() {
    return newPassword1;
  }

  public void setNewPassword1(String newPassword1) {
    this.newPassword1 = newPassword1;
  }
}
