package com.egs.bank.dto;

public class TransactionInfoDto {
  private Long poNumber;
  private Long discount;
  private Long paymentTerms;
  private String reference;
  private String paymentStatus;
  private String type;
  private Boolean clientContactExclusive;
  private Boolean collectOnContract;
  private String info;
  private String currency;
  private String billerData1;
  private String billerData2;
  private Boolean arrears;
  private Boolean testMode;

  public TransactionInfoDto() {}

  public TransactionInfoDto(
      Long poNumber,
      Long discount,
      Long paymentTerms,
      String reference,
      String paymentStatus,
      String type,
      Boolean clientContactExclusive,
      Boolean collectOnContract,
      String info,
      String currency,
      String billerData1,
      String billerData2,
      Boolean arrears,
      Boolean testMode) {
    this.poNumber = poNumber;
    this.discount = discount;
    this.paymentTerms = paymentTerms;
    this.reference = reference;
    this.paymentStatus = paymentStatus;
    this.type = type;
    this.clientContactExclusive = clientContactExclusive;
    this.collectOnContract = collectOnContract;
    this.info = info;
    this.currency = currency;
    this.billerData1 = billerData1;
    this.billerData2 = billerData2;
    this.arrears = arrears;
    this.testMode = testMode;
  }

  public Long getPoNumber() {
    return poNumber;
  }

  public void setPoNumber(Long poNumber) {
    this.poNumber = poNumber;
  }

  public Long getDiscount() {
    return discount;
  }

  public void setDiscount(Long discount) {
    this.discount = discount;
  }

  public Long getPaymentTerms() {
    return paymentTerms;
  }

  public void setPaymentTerms(Long paymentTerms) {
    this.paymentTerms = paymentTerms;
  }

  public String getReference() {
    return reference;
  }

  public void setReference(String reference) {
    this.reference = reference;
  }

  public String getPaymentStatus() {
    return paymentStatus;
  }

  public void setPaymentStatus(String paymentStatus) {
    this.paymentStatus = paymentStatus;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public Boolean getClientContactExclusive() {
    return clientContactExclusive;
  }

  public void setClientContactExclusive(Boolean clientContactExclusive) {
    this.clientContactExclusive = clientContactExclusive;
  }

  public Boolean getCollectOnContract() {
    return collectOnContract;
  }

  public void setCollectOnContract(Boolean collectOnContract) {
    this.collectOnContract = collectOnContract;
  }

  public String getInfo() {
    return info;
  }

  public void setInfo(String info) {
    this.info = info;
  }

  public String getCurrency() {
    return currency;
  }

  public void setCurrency(String currency) {
    this.currency = currency;
  }

  public String getBillerData1() {
    return billerData1;
  }

  public void setBillerData1(String billerData1) {
    this.billerData1 = billerData1;
  }

  public String getBillerData2() {
    return billerData2;
  }

  public void setBillerData2(String billerData2) {
    this.billerData2 = billerData2;
  }

  public Boolean getArrears() {
    return arrears;
  }

  public void setArrears(Boolean arrears) {
    this.arrears = arrears;
  }

  public Boolean getTestMode() {
    return testMode;
  }

  public void setTestMode(Boolean testMode) {
    this.testMode = testMode;
  }
}
