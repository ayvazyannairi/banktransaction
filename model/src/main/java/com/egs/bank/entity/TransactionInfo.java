package com.egs.bank.entity;

import javax.persistence.*;

@Entity
@Table(name = "trn")
public class TransactionInfo {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id")
  private Long id;

  @Column(name = "info")
  private String infoJSON;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getInfoJSON() {
    return infoJSON;
  }

  public void setInfoJSON(String info) {
    this.infoJSON = info;
  }
}
