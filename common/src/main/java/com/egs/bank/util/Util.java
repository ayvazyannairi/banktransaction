package com.egs.bank.util;

import java.util.Calendar;
import java.util.Date;
import java.util.Random;

/**
 * Helper Class.
 * */
public class Util {

  /**
   * Calculated the date.
   * */
  public static Date calculateDate() {
    Date date = new Date();
    Calendar calendar = Calendar.getInstance();
    calendar.setTime(date);
    calendar.add(Calendar.YEAR, 1);
    date = calendar.getTime();

    return date;
  }

  /**
   * Generated random string.
   * */
  public static String randomString(Integer size) {
    int leftLimit = 97; // letter 'a'
    int rightLimit = 122; // letter 'z'
    int targetStringLength = size;
    Random random = new Random();

    return random.ints(leftLimit, rightLimit + 1)
        .limit(targetStringLength)
        .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
        .toString();
  }

  /**
   * Generated random string of integer like this: 1234 5678 9101 1121, if bin = 1234 and length=16.
   * */
  public static String generate(String bin, int length) {
    Random random = new Random(System.currentTimeMillis());
    int randomNumberLength = length - (bin.length() + 1);

    StringBuilder builder = new StringBuilder(bin);
    for (int i = 0; i < randomNumberLength; i++) {
      int digit = random.nextInt(10);
      builder.append(digit);
    }

    int checkDigit = getCheckDigit(builder.toString());
    builder.append(checkDigit);
    StringBuilder newBuilder = new StringBuilder();
    for (int i = 0; i < builder.length(); i++) {
      newBuilder.append(builder.charAt(i));
      if ((i + 1) % 4 == 0) {
        newBuilder.append(' ');

      }
    }
    return newBuilder.toString();
  }

  private static int getCheckDigit(String number) {
    int sum = 0;
    for (int i = 0; i < number.length(); i++) {

      // Get the digit at the current position.
      int digit = Integer.parseInt(number.substring(i, (i + 1)));

      if ((i % 2) == 0) {
        digit = digit * 2;
        if (digit > 9) {
          digit = (digit / 10) + (digit % 10);
        }
      }
      sum += digit;
    }

    // The check digit is the number required to make the sum a multiple of
    // 10.
    int mod = sum % 10;
    return ((mod == 0) ? 0 : 10 - mod);
  }
}
