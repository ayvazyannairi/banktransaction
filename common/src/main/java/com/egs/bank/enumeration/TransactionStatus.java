package com.egs.bank.enumeration;

/**
 * Transaction Status.
 * */
public enum TransactionStatus {
    PENDING, ACCEPTED, REJECTED
}
