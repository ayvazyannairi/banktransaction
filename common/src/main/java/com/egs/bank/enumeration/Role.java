package com.egs.bank.enumeration;

/**
 * Roles.
 * */
public enum Role {
    ROLE_ADMIN, ROLE_USER
}
