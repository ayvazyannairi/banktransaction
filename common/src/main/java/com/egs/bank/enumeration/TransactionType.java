package com.egs.bank.enumeration;

/**
 * Transaction Type.
 * */
public enum TransactionType {
    DEPOSIT, WITHDRAWAL
}
