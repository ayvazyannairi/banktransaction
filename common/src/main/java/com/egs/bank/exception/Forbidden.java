package com.egs.bank.exception;

/**
 * Throw Runtime Exception.
 * */
public class Forbidden extends RuntimeException {
}
