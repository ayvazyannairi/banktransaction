package com.egs.bank.exception;

/**
 * Throw Runtime Exception.
 * */
public class NotFound extends RuntimeException {
}
