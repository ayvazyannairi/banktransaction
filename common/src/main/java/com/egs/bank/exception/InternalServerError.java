package com.egs.bank.exception;

/**
 * Throw Runtime Exception.
 * */
public class InternalServerError extends RuntimeException {
}
