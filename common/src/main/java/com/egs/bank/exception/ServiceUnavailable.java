package com.egs.bank.exception;

/**
 * Throw Runtime Exception.
 * */
public class ServiceUnavailable extends RuntimeException {
}
