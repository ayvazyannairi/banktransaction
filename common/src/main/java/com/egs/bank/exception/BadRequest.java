package com.egs.bank.exception;

/**
 * Throw Runtime Exception.
 * */
public class BadRequest extends RuntimeException {
}
