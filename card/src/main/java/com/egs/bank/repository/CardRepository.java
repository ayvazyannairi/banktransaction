package com.egs.bank.repository;

import com.egs.bank.entity.Card;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * .
 * */
@Repository
@Transactional
public interface CardRepository extends JpaRepository<Card, Long> {

  Card findCardByCardNumber(String cardNumber);
}
