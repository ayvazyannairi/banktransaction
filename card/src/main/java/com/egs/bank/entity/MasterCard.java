package com.egs.bank.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.time.LocalDate;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * .
 * */
@Entity
@DiscriminatorValue(value = "master")
@JsonIgnoreProperties({"hibernateLazyInitializer"})
public class MasterCard extends Card {

  @Column(name = "card_type")
  private String cardType;

  @Column(name = "CVV")
  private int cvv;

  @Column(name = "valid_date")
  private LocalDate validDate;

  public String getCardType() {
    return cardType;
  }

  public void setCardType(String cardType) {
    this.cardType = cardType;
  }

  public int getCvv() {
    return cvv;
  }

  public void setCvv(int cvv) {
    this.cvv = cvv;
  }

  public LocalDate getValidDate() {
    return validDate;
  }

  public void setValidDate(LocalDate validDate) {
    this.validDate = validDate;
  }
}