package com.egs.bank.controller;

import com.egs.bank.entity.Card;
import com.egs.bank.service.CardService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * .
 * */
@RestController
@RequestMapping("/api/cardsModule")
public class CardControllerModule {

  private final CardService cardService;

  public CardControllerModule(CardService cardService) {
    this.cardService = cardService;
  }

  @PostMapping(value = "/createMasterCardd/{id}")
  public ResponseEntity<Card> createMasterCard(@PathVariable(value = "id") Long id) {
    System.out.println("hello");
    return cardService.createCard(id, "MasterCard");
  }
}