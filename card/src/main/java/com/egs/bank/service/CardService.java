package com.egs.bank.service;

import com.egs.bank.entity.Card;
import com.egs.bank.entity.MasterCard;
import com.egs.bank.entity.User;
import com.egs.bank.entity.VisaCard;
import com.egs.bank.repository.CardRepository;
import com.egs.bank.repository.UserRepository;
import com.egs.bank.util.Util;
import java.time.LocalDate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

/**
 * .
 * */
@Service

public class CardService {
  private final UserRepository userRepository;
  private final CardRepository cardRepository;

  public CardService(UserRepository userRepository, CardRepository cardRepository) {
    this.userRepository = userRepository;
    this.cardRepository = cardRepository;
  }

  /**
   * Created Card required user ID and choosing MasterCard or VisaCard.
   * */
  public ResponseEntity<Card> createCard(Long userId, String cardName) {
    //Default amount for user card.
    int amount = 0;
    //Generated random cvv for card.
    int cvv = Integer.parseInt(Util.generate("", 3));

    User user = userRepository.findUserById(userId);

    LocalDate date = LocalDate.now();
    if (cardName.equals("MasterCard")) {
      MasterCard masterCard = new MasterCard();
      boolean exists = false;
      while (!exists) {
        String cardNumber = Util.generate("4318", 16);
        Card card = cardRepository.findCardByCardNumber(cardNumber);
        if (card == null) {
          masterCard.setUser(user);
          masterCard.setCardNumber(cardNumber);
          masterCard.setCvv(cvv);
          masterCard.setCardType(cardName);
          masterCard.setValidDate(date.plusYears(3));
          masterCard.setAmount(amount);
          cardRepository.save(masterCard);
          exists = true;
        }
      }
      return new ResponseEntity<>(masterCard, HttpStatus.OK) ;
    } else if (cardName.equals("VisaCard")) {
      VisaCard visaCard = new VisaCard();
      boolean exists = false;
      while (!exists) {
        String cardNumber = Util.generate("4033", 16);
        Card card = cardRepository.findCardByCardNumber(cardNumber);
        if (card == null) {
          visaCard.setCardNumber(cardNumber);
          visaCard.setUser(user);
          visaCard.setCvv(cvv);
          visaCard.setCardType(cardName);
          visaCard.setValidDate(date.plusYears(3));
          visaCard.setAmount(amount);
          cardRepository.save(visaCard);
          exists = true;
        }
      }
      return new ResponseEntity<>(visaCard, HttpStatus.OK) ;
    } else {
      return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST) ;
    }
  }
}
