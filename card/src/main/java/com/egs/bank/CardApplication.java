package com.egs.bank;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

/**
 * Main method.
 */
@SpringBootApplication
public class CardApplication {
  @Bean
  public RestTemplate getRestTemplate() {
    return new RestTemplate();
  }
  private static final Logger logger = LoggerFactory.getLogger(CardApplication.class);

  /**
   * .
   * */
  public static void main(String[] args) {
   logger.info("Logger is running");
    SpringApplication.run(CardApplication.class,args);
    logger.info("Logger is run successfully");
  }
}
