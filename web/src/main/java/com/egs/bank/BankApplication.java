package com.egs.bank;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.client.RestTemplate;

/**
 * Main method.
 */
@SpringBootApplication
public class BankApplication {

  @Bean
  public RestTemplate getRestTemplate() {
    return new RestTemplate();
  }
 @SuppressWarnings("checkstyle:Indentation")
  private static final Logger logger = LoggerFactory.getLogger(BankApplication.class);

  /**
   * .
   * */
  @SuppressWarnings("checkstyle:Indentation")
  public static void main(String[] args) {
    logger.info("Logger is running");
    SpringApplication.run(BankApplication.class, args);
    logger.info("Logger is run successfully");

    BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
    System.out.println(encoder.encode("David1984"));

  }
}
