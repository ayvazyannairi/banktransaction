package com.egs.bank.controller;

import com.egs.bank.entity.Card;
import com.egs.bank.entity.MasterCard;
import com.egs.bank.service.CardService;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collections;
import java.util.Map;
import java.util.Set;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/**
 * .
 * */
@RestController
@RequestMapping("/api/cards")
public class CardController {

  private final RestTemplate restTemplate;
  private final CardService cardService;

  public CardController(CardService cardService,RestTemplate restTemplate) {
    this.cardService = cardService;
    this.restTemplate = restTemplate;
  }

  /**
   * .
   * */
  @PostMapping(value = "/createMasterCard/{id}")
  public ResponseEntity<MasterCard> createMasterCard(@PathVariable(value = "id") Long id) throws URISyntaxException {
    long userId = id;
    String url = "http://localhost:8081/api/cardsModule/createMasterCardd/" + userId;
    MasterCard masterCard = restTemplate.getForObject(new URI(url),  MasterCard.class);
    if (masterCard != null) {
      return new ResponseEntity<>(masterCard, HttpStatus.OK);
    }else {
      return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
    }
  }

  @PostMapping(value = "/createVisaCard/{id}")
//  @PreAuthorize(value = "")
  public ResponseEntity<Card> createVisaCard(@PathVariable(value = "id") Long id) {
    return cardService.createCard(id, "VisaCard");
  }

  @GetMapping(value = "/")
//  @PreAuthorize(value = "")
  public ResponseEntity<Set<Card>> getUserAllCards(@RequestHeader String token) {
    return cardService.getCard(token);
  }

  @DeleteMapping(value = "/delete")
//  @PreAuthorize(value = "")
  public String deleteUserCard(@RequestBody Card card) {
    return cardService.deleteCard(card.getCardNumber());
  }

  @GetMapping(value = "/excel")
  @ResponseStatus(HttpStatus.OK)
//  @PreAuthorize(value = "")
  public Map<String, String> excel() {
    if(cardService.generateExcel("visaCard").equals("File created Successfully")) {
      return Collections.singletonMap("message", "ok.");
    }
    return Collections.singletonMap("message", "problem");
  }
}
