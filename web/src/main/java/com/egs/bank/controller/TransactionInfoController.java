package com.egs.bank.controller;

import com.egs.bank.dto.TransactionInfoDto;
import com.egs.bank.service.TransactionInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/transaction-info")
public class TransactionInfoController {
  private final TransactionInfoService transactionInfoService;

  @Autowired
  public TransactionInfoController(TransactionInfoService transactionInfoService) {
    this.transactionInfoService = transactionInfoService;
  }

  @PostMapping("/create")
  public ResponseEntity create(@RequestBody TransactionInfoDto transactionInfoDTO) {
    transactionInfoService.create(transactionInfoDTO);
    return ResponseEntity.ok(HttpStatus.CREATED);
  }

  @GetMapping("/get/{id}")
  public ResponseEntity<TransactionInfoDto> get(@PathVariable(name = "id") Long id) {
    TransactionInfoDto transactionInfoDTO = transactionInfoService.getTransactionInfo(id);
    if (transactionInfoDTO != null) {
      return new ResponseEntity<>(transactionInfoDTO, HttpStatus.OK);
    } else {
      return new ResponseEntity<>(null, HttpStatus.NOT_ACCEPTABLE);
    }
  }
}
