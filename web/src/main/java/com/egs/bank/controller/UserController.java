package com.egs.bank.controller;

import com.egs.bank.dto.UserDto;
import com.egs.bank.entity.Transaction;
import com.egs.bank.entity.User;
import com.egs.bank.entity.UserKey;
import com.egs.bank.exception.BadRequest;
import com.egs.bank.service.TransactionService;
import com.egs.bank.service.UserService;
import com.egs.bank.util.Util;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

/**
 * .
 */
@RestController
@RequestMapping("/api/users")
@Api(value = "Controller for Swagger")
public class UserController {
  private static final Logger logger = LoggerFactory.getLogger(UserController.class);
  private final UserService userService;

  private final TransactionService transactionService;

  public UserController(UserService userService, TransactionService transactionService) {
    this.userService = userService;
    this.transactionService = transactionService;
  }

  @GetMapping(value = "/{id}/transactions")
  @ApiOperation(value = "get transaction by id")
  public Map<String, List<Transaction>> getUserTransactions(@PathVariable(value = "id") Long id) {
    List<Transaction> transactions = transactionService.getUserTransactions(id);
    return Collections.singletonMap("message", transactions);
  }

  @GetMapping(value = "/{id}/transactions/{date}")
  public Map<String, List<Transaction>> getUserTransactionsByDate(@PathVariable(value = "id") Long id,
                                                                  @PathVariable(value = "date")
                                                                  @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") Date createdDate) {
    List<Transaction> transactions = transactionService.getUserTransactionsByDate(id, createdDate);
    return Collections.singletonMap("message", transactions);
  }

  /**
   * .
   */
  //@PreAuthorize("hasRole('ROLE_ADMIN')")
  @PutMapping(value = "/{id}/roles")
  @ResponseStatus(HttpStatus.OK)
  public Map<String, String> changeUserRole(@PathVariable(value = "id") Long id) {
    UserKey userKey = userService.getUserKey(id);
    if (userKey == null) {
      throw new BadRequest();
    } else {
      userService.updateUserRole(userKey);
    }
    return Collections.singletonMap("message", "ok.");
  }

  /**
   * .
   */
  @PostMapping(value = "/restorePassword")
  @ResponseStatus(HttpStatus.OK)
  public Map<String, String> restorePassword(@RequestBody User user) {
    User user1 = userService.findUserByEmail(user.getEmail());
    if (user1 == null) {
      throw new BadRequest();
    } else {
      String newPassword = Util.randomString(10);
      userService.resetPassword(newPassword, user1);
      userService.sendEmail(user1.getEmail(), newPassword);
    }
    return Collections.singletonMap("message", "ok.");
  }

  /**
   * .
   */
  @PostMapping(value = "/changePass")
  @ResponseStatus(HttpStatus.OK)
  public Map<String, String> changePass(@RequestBody UserDto userDto) {
    User userByEmail = userService.getUserByEmail(userDto.getEmail());
    if (userByEmail == null) {
      logger.error("error");
      throw new BadRequest();
    } else if (userService.changePassword(userDto)) {
      logger.info("password already  changed");
      return Collections.singletonMap("message", "ok.");
    } else {
      return Collections.singletonMap("message", "no.");
    }
  }

}