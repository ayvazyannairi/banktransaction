# General Info 

This is a bank transactions project based on spring-boot application.

## Modules

The main parts of the project are:

* common: contains common and reusable classes regarding exceptions, enumeration, utility
* model: contains information regarding database migrations and orm entities
* repository: contains interfaces for database interaction
* service: contains business logic of project and also security part
* web: contains controllers and servlet initializer

## Spring Active Profiles (Local)

Local development property file is application.properties.

## How to build

First git clone https://gitlab.com/Lyov/bank.git

Second it is necessary to create mysql database. The database name by default should be bank.

Third it is necessary to move into model sub project and run the following command.

    mvn clean flyway:migrate

To build all the modules run in the project root directory the following command with Maven 3:

    mvn clean install

## Testing

There are two levels of testing contained in the project unit test and integration test.

