package com.egs.bank.repository;

import com.egs.bank.entity.Card;
import com.egs.bank.entity.MasterCard;
import com.egs.bank.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Set;

/** . */
@Repository
@Transactional
public interface CardRepository extends JpaRepository<Card, Long> {

  Card findCardByCardNumber(String cardNumber);

  Set<MasterCard> findCardByUser(User user);
}
