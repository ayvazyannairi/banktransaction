package com.egs.bank.repository;

import com.egs.bank.entity.Account;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;


/**
 * This is Repository.
 * */
@SuppressWarnings("All")
@Repository
@Transactional
public interface AccountRepository extends JpaRepository<Account, Long> {

  List<Account> findByUserId(Long userId);

}
