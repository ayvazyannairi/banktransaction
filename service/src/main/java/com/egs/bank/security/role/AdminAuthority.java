package com.egs.bank.security.role;

import com.egs.bank.enumeration.Role;
import org.springframework.security.core.GrantedAuthority;

/**
 * For Admin Authorization.
 */
@SuppressWarnings("All")
public class AdminAuthority implements GrantedAuthority {

  @Override
  public String getAuthority() {
    return Role.ROLE_ADMIN.name();
  }
}
