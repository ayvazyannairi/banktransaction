package com.egs.bank.service;

import com.egs.bank.entity.Account;
import com.egs.bank.exception.InternalServerError;
import com.egs.bank.repository.AccountRepository;
import java.util.List;
import org.springframework.stereotype.Service;


/**
 * This is Service.
 */
@SuppressWarnings("ALL")
@Service
public class AccountService {

    private final AccountRepository accountRepository;

    public AccountService(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

  /**
   * Generated an Account.
   * */
  public Account generateAccount(Account account) {
    try {
      account = this.accountRepository.save(account);
    } catch (Exception ex) {
      throw new InternalServerError();
    }
    return account;
  }

  /**
   * The method return list Accounts of User filter by ID.
   * */
  public List<Account> getAccountsByUserId(Long userId) {
    List<Account> accounts;
    try {
      accounts = this.accountRepository.findByUserId(userId);
    } catch (Exception ex) {
      throw new InternalServerError();
    }
    return accounts;
  }

}
