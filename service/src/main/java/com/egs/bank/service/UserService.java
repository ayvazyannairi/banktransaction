package com.egs.bank.service;

import com.egs.bank.dto.UserDto;
import com.egs.bank.entity.User;
import com.egs.bank.entity.UserKey;
import com.egs.bank.enumeration.Role;
import com.egs.bank.exception.InternalServerError;
import com.egs.bank.repository.UserKeyRepository;
import com.egs.bank.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.validation.constraints.Email;

/**
 * .
 * */
@SuppressWarnings("ALl")
@Service
public class UserService {
  private static final Logger logger = LoggerFactory.getLogger(UserService.class);

  @Autowired
  private JavaMailSender javaMailSender;

  private final UserRepository userRepository;

  private final UserKeyRepository userKeyRepository;

  public UserService(UserRepository userRepository, UserKeyRepository userKeyRepository) {
    this.userRepository = userRepository;
    this.userKeyRepository = userKeyRepository;
  }

  /**
   * .
   * */
  public User getUserByIdAndUserUuid(Long id, String userUuid) {
    User user = this.userRepository.findByIdAndUuid(id, userUuid);
    user.setUsername(user.getUsername());
    user.setEmail(user.getEmail());
    user.setPassword(user.getPassword());
    return user;
  }

  /**
   * Method updated the UserKey.
   * */
  public void updateUserRole(UserKey userKey) {
    if (userKey.getRole().equals(Role.ROLE_ADMIN)) {
      userKey.setRole(Role.ROLE_USER);
    } else {
      userKey.setRole(Role.ROLE_ADMIN);
    }
    userKeyRepository.save(userKey);
  }

  /**
   * Method return User object filter by ID.
   * */
  public User getUserById(Long id) {
    User user;
    try {
      user = this.userRepository.getById(id);
    } catch (Exception ex) {
      throw new InternalServerError();
    }
    return user;
  }

  /**
   * Method return User object filter by Email.
   * */
  public User getUserByEmail(String email) {
    @Email
    String email1 = email;
    User user;
    try {
      user = this.userRepository.findByEmail(email1);
    } catch (Exception ex) {
      throw new InternalServerError();
    }

    return user;
  }

  /**
   * Method return generated User.
   * */
  public User generateUser(User user) {
    try {
      user = this.userRepository.save(user);
    } catch (Exception ex) {
      throw new InternalServerError();
    }

    return user;
  }

  /**
   * Method generate UserKey object.
   * */
  public void generateUserKey(UserKey userKey) {
    try {
      this.userKeyRepository.save(userKey);
    } catch (Exception ex) {
      throw new InternalServerError();
    }
  }

  /**
   * Method return UserKey object.
   * */
  public UserKey getUserKey(Long userId) {
    UserKey userKey;

    try {
      userKey = this.userKeyRepository.findByUserId(userId);
    } catch (Exception ex) {
      throw new InternalServerError();
    }

    return userKey;
  }

  /**
   * Method find User and return the object filter by Email.
   * */
  public User findUserByEmail(String email) {
    return userRepository.findByEmail(email);
  }

  /**
   * Method save User object.
   * */
  public void save(User user) {
    userRepository.save(user);
  }

  /**
   * Method send Email to User, then the User forgot password.
   * */
  public void sendEmail(String email2, String newPassword) {
    SimpleMailMessage msg = new SimpleMailMessage();
    msg.setTo(email2);
    msg.setFrom("egstesttesttest@gmail.com");
    msg.setSubject("New Password");
    msg.setText("Your password has been changed, here is your new password "
        + newPassword + ", please change your password after logging in.");
    javaMailSender.send(msg);
  }

  /**
   * Method for reset the password.
   * */
  public void resetPassword(String newPassword, User user) {
    BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
    String encryptedNewPass = passwordEncoder.encode(newPassword);
    user.setPassword(encryptedNewPass);
    userRepository.save(user);
  }

  /**
   * Method changed the password.
   * */
  public boolean changePassword1(UserDto userDto) {
    if (!userDto.getNewPassword().equals(userDto.getNewPassword1())) {
      return false;
    }
    User userByEmail = userRepository.findByEmail(userDto.getEmail());
    if (userByEmail == null) {
      return false;
    } else {
      BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
      String armenDataBase = userByEmail.getPassword();
      if (passwordEncoder.matches(userDto.getPassword(), armenDataBase)) {
        String encodeNewPass = passwordEncoder.encode(userDto.getNewPassword());
        userByEmail.setPassword(encodeNewPass);
        userRepository.save(userByEmail);
      } else {
        return false;
      }
    }
    return true;
  }

  /**
   * Method return true or false, if changed password.
   * */
  public boolean changePassword(UserDto userDto) {
    if (!userDto.getNewPassword().equals(userDto.getNewPassword1())) {
//      logger.info("write new password correctly");
      return false;
    }
    User userByEmail = userRepository.findByEmail(userDto.getEmail());
    if (userByEmail == null) {
      logger.info("user does not exist");
      return false;
    } else {
      BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
      String armenDataBase = userByEmail.getPassword();
      if (passwordEncoder.matches(userDto.getPassword(), armenDataBase)) {
        String encodeNewPass = passwordEncoder.encode(userDto.getNewPassword());
        userByEmail.setPassword(encodeNewPass);
        userRepository.save(userByEmail);
      } else {
        return false;
      }
    }
    logger.info("password  changed");
    return true;
  }
}
