package com.egs.bank.service;

import com.egs.bank.property.CredentialProperty;
import com.egs.bank.security.model.Credential;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Base64;
import java.util.Date;
import org.springframework.stereotype.Service;


/**
 * .
 */
@Service
public class JsonWebTokenService {

  private final CredentialProperty credentialProperty;

  public JsonWebTokenService(CredentialProperty credentialProperty) {
    this.credentialProperty = credentialProperty;
  }

  /**
   * Generated JWT Token.
   * */
  public String generateJwt(Long userId, int period, String uuid) {
    LocalDateTime now = LocalDateTime.now();
    ZonedDateTime expirationDate = now.plusDays(period).atZone(ZoneId.systemDefault());

    return Jwts.builder()
        .setIssuer(this.credentialProperty.getProvider())
        .setSubject(String.valueOf(userId))
        .setAudience(this.credentialProperty.getSubject())
        .setId(uuid)
        .setExpiration(Date.from(expirationDate.toInstant()))
        .signWith(SignatureAlgorithm.HS512, this.credentialProperty.getKey().getBytes())
        .compact();
  }

  /**
   * Verification JWT Token.
   * */
  public boolean verifyJwt(String jwt) {
    try {
      Jwts.parser().setSigningKey(this.credentialProperty.getKey().getBytes()).parseClaimsJws(jwt);

      return true;
    } catch (Exception e) {
      e.printStackTrace();
    }
    return false;
  }

  /**
   * Decoding JWT Token.
   * */
  public Credential decodeJwt(String jwt) {
    try {
      ObjectMapper mapper = new ObjectMapper();

      String[] jwtSections = jwt.split("\\.");
      String claim = new String(Base64.getDecoder().decode(jwtSections[1]));

      return mapper.<Credential>readValue(claim, Credential.class);
    } catch (Exception e) {
      e.printStackTrace();
    }

    return new Credential();
  }
}
