package com.egs.bank.service;

import com.egs.bank.dto.TransactionInfoDto;
import com.egs.bank.entity.TransactionInfo;
import com.egs.bank.repository.TransactionInfoRepository;
import javax.persistence.EntityNotFoundException;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TransactionInfoService {
  private final TransactionInfoRepository transactionInfoRepository;

  @Autowired
  public TransactionInfoService(TransactionInfoRepository transactionInfoRepository) {
    this.transactionInfoRepository = transactionInfoRepository;
  }

  public String jsonToString(TransactionInfoDto transactionInfoDTO) {
    GsonBuilder gsonBuilder = new GsonBuilder();
    gsonBuilder.setDateFormat("yyyy-MM-dd'T'hh:mm:ss'Z'");
    Gson gson = gsonBuilder.create();
    String transactionInfoForDb = gson.toJson(transactionInfoDTO);
    return transactionInfoForDb;
  }

  public TransactionInfoDto toDTO(String info) {
    GsonBuilder gsonBuilder = new GsonBuilder();
    gsonBuilder.setDateFormat("yyyy-MM-dd'T'hh:mm:ss'Z'");
    Gson gson = gsonBuilder.create();
    TransactionInfoDto transactionInfoDto = gson.fromJson(info, TransactionInfoDto.class);
    return transactionInfoDto;
  }

  public void create(TransactionInfoDto transactionInfoDTO) {
    String info = jsonToString(transactionInfoDTO);
    TransactionInfo transactionInfo = new TransactionInfo();
    transactionInfo.setInfoJSON(info);
    transactionInfoRepository.save(transactionInfo);
  }

  public TransactionInfoDto getTransactionInfo(Long id) {
    TransactionInfoDto transactionInfoDto = new TransactionInfoDto();
    TransactionInfo transactionInfo =
        transactionInfoRepository.findById(id).orElseThrow(EntityNotFoundException::new);
    String info = transactionInfo.getInfoJSON();
    transactionInfoDto = toDTO(info);
    return transactionInfoDto;
  }
}
