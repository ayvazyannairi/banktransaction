package com.egs.bank.service;

import com.egs.bank.entity.Card;
import com.egs.bank.entity.MasterCard;
import com.egs.bank.entity.User;
import com.egs.bank.entity.VisaCard;
import com.egs.bank.repository.CardRepository;
import com.egs.bank.repository.UserRepository;
import com.egs.bank.util.Util;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import com.itextpdf.layout.element.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

/**
 * .
 */
@Service
public class CardService {
  private final UserRepository userRepository;
  private final CardRepository cardRepository;

  public CardService(UserRepository userRepository, CardRepository cardRepository) {
    this.userRepository = userRepository;
    this.cardRepository = cardRepository;
  }

  /**
   * Created Card required user ID and choosing MasterCard or VisaCard.
   */
  public ResponseEntity<Card> createCard(Long userId, String cardName) {
    // Default amount for user card.
    int amount = 0;
    // Generated random cvv for card.
    int cvv = Integer.parseInt(Util.generate("", 3));

    User user = userRepository.findUserById(userId);

    LocalDate date = LocalDate.now();
    if (cardName.equals("MasterCard")) {
      MasterCard masterCard = new MasterCard();
      boolean exists = false;
      while (!exists) {
        String cardNumber = Util.generate("4318", 16);
        Card card = cardRepository.findCardByCardNumber(cardNumber);
        if (card == null) {
          masterCard.setUser(user);
          masterCard.setCardNumber(cardNumber);
          masterCard.setCvv(cvv);
          masterCard.setCardType(cardName);
          masterCard.setValidDate(date.plusYears(3));
          masterCard.setAmount(amount);
          cardRepository.save(masterCard);
          exists = true;
        }
      }
      return new ResponseEntity<>(HttpStatus.OK);
    } else if (cardName.equals("VisaCard")) {
      VisaCard visaCard = new VisaCard();
      boolean exists = false;
      while (!exists) {
        String cardNumber = Util.generate("4033", 16);
        Card card = cardRepository.findCardByCardNumber(cardNumber);
        if (card == null) {
          visaCard.setCardNumber(cardNumber);
          visaCard.setUser(user);
          visaCard.setCvv(cvv);
          visaCard.setCardType(cardName);
          visaCard.setValidDate(date.plusYears(3));
          visaCard.setAmount(amount);
          cardRepository.save(visaCard);
          exists = true;
        }
      }
      return new ResponseEntity<>(HttpStatus.OK);
    } else {
      return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }
  }

  /**
   * .
   */
  public ResponseEntity<Set<Card>> getCard(String token) {
    User user = userRepository.findByResetToken(token);
    Set<Card> card = user.getCards();
    return new ResponseEntity<>(card, HttpStatus.OK);
  }

  /**
   * .
   */
  public String deleteCard(String cardNumber) {
    Card card = cardRepository.findCardByCardNumber(cardNumber);
    if (card != null) {
      cardRepository.delete(card);
      return "The card was deleted successfully․";
    }
    return "No data was found with the specified userId or cardId.";
  }

  public ResponseEntity<String> generatePDF(Long userId) {
    String filePath = "/home/saten/Desktop/PDFReport";
    PdfDocument pdfDoc = null;
    try {
      pdfDoc = new PdfDocument(new PdfWriter(filePath));
    } catch (FileNotFoundException e) {
      System.err.println("File not found");
    }

    assert pdfDoc != null;
    Document document = new Document(pdfDoc);

    float[] pointColumnWidths = {50F, 50F, 50F, 50F, 75F, 140F}; // , 40F, 70F
    Table table = new Table(pointColumnWidths);

    table.addCell(new Cell().add(new Paragraph("User-Id")));
    table.addCell(new Cell().add(new Paragraph("Card-Id")));
    table.addCell(new Cell().add(new Paragraph("Payment-Mode")));
    table.addCell(new Cell().add(new Paragraph("Amount")));
    table.addCell(new Cell().add(new Paragraph("Card-Type")));
    table.addCell(new Cell().add(new Paragraph("Card-Number")));
    //    table.addCell(new Cell().add(new Paragraph("CVV")));
    //    table.addCell(new Cell().add(new Paragraph("Valid-Date")));

    Set<MasterCard> masterCardInfo = cardRepository.findCardByUser(userRepository.findUserById(userId));
    String cardUserId = String.valueOf(userId);
    String cardId;
    String paymentMode = "Master";
    String amount;
    String cardType;
    String cardNumber;
    //    String cvv;
    //    String validDate;

    for (MasterCard card : masterCardInfo) {
      if(!card.getCardType().equals("MasterCard")) continue;
      cardType = String.valueOf(card.getCardType());
      cardId = String.valueOf(card.getId());
      amount = String.valueOf(card.getAmount());
      cardNumber = String.valueOf(card.getCardNumber());
      table.addCell(new Cell().add(new Paragraph(cardUserId)));
      table.addCell(new Cell().add(new Paragraph(cardId)));
      table.addCell(new Cell().add(new Paragraph(paymentMode)));
      table.addCell(new Cell().add(new Paragraph(amount)));
      table.addCell(new Cell().add(new Paragraph(cardType)));
      table.addCell(new Cell().add(new Paragraph(cardNumber)));
    }

    document.add(table);
    document.close();
    return new ResponseEntity<>("PDF has been successfully created", HttpStatus.CREATED);
  }

  /**
   * Generates excel file from all visa cards.
   * */
  public String generateExcel(String cardName) {
    if (cardName.equals("visaCard")) {
      List<VisaCard> allCards = getAllVisaCards();
      try {
        Workbook workbook = new XSSFWorkbook();
        Sheet sheet = workbook.createSheet("cards");
        Row header = sheet.createRow(0);
        createHeaderList(header);
        for (int i = 0; i < allCards.size(); i++) {
          Row row = sheet.createRow(i + 1);
          createList(allCards.get(i), row);
        }

        FileOutputStream out = new FileOutputStream(
            new File("C:\\VisaCardExcels\\AllVisaCards.xlsx"));
        workbook.write(out)
        ;
        out.close();
        return "File created Successfully";
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
    return null;
  }

  /**
   * Method return all visa cards.
   * */
  public List<VisaCard> getAllVisaCards() {
    List<VisaCard> cards = new ArrayList<>();
    //return visaCardRepository.findAll();
    User user = new User();
    user.setEmail("a@gmail.com");
    VisaCard v = new VisaCard();
    v.setCardType("platinum");
    v.setId(5);
    v.setAmount(500);
    v.setCardNumber("12345678901234");
    v.setUser(user);
    cards.add(v);
    return cards;
  }

  private static void createHeaderList(Row row) {
    org.apache.poi.ss.usermodel.Cell cell = row.createCell(0);
    cell.setCellValue("ID");

    cell = row.createCell(1);
    cell.setCellValue("Amount");

    cell = row.createCell(2);
    cell.setCellValue("CVV");

    cell = row.createCell(3);
    cell.setCellValue("Card Type");

    cell = row.createCell(4);
    cell.setCellValue("Card Number");

    cell = row.createCell(5);
    cell.setCellValue("Valid Date");

    cell = row.createCell(6);
    cell.setCellValue("User's Email");

  }

  private static void createList(VisaCard visaCard, Row row) {
    org.apache.poi.ss.usermodel.Cell cell = row.createCell(0);
    cell.setCellValue(visaCard.getId());

    cell = row.createCell(1);
    cell.setCellValue(visaCard.getAmount());

    cell = row.createCell(2);
    cell.setCellValue(visaCard.getCvv());

    cell = row.createCell(3);
    cell.setCellValue(visaCard.getCardType());

    cell = row.createCell(4);
    cell.setCellValue(visaCard.getCardNumber());

    cell = row.createCell(5);
    cell.setCellValue(String.valueOf(visaCard.getValidDate()));

    cell = row.createCell(6);
    cell.setCellValue(visaCard.getUser().getEmail());
  }
}
